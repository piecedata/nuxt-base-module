// import { resolve, join } from 'path';
import deepmerge from 'deepmerge';
// const fsExtra = require('fs-extra');

export default function (moduleOptions) {
  const options = {
    ...moduleOptions,
    ...this.options.nuxtBaseModule,
  };

  // expose the namespace / set a default
  if (!options.namespace) {
    options.namespace = 'nuxtBaseModule';
  }
  const { namespace } = options;

  this.options.env.API_URL = process.env.API_URL;

  this.options.build.loaders.cssModules = deepmerge(this.options.build.loaders.cssModules, {
    modules: {
      localIdentName: process.env.NODE_ENV === 'production'
        ? '[hash:base64:5]'
        : '[path][name]-[local]-[hash:base64:5]',
    },
    localsConvention: 'camelCase',
  });

  if (!this.options.pwa) {
    this.options.pwa = {};
  }

  this.options.pwa = deepmerge({
    manifest: {
      name: process.env.APP_NAME,
      short_name: process.env.APP_NAME,
      description: process.env.APP_DESCRIPTION,
    },
  }, this.options.pwa);

  this.options.router = deepmerge(this.options.router, {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-exact-active',
  });

  this.options.modules = [
    ...this.options.modules,
    'nuxt-preserve-scroll',
    'vue-scrollto/nuxt',
    'nuxt-toast',
  ];

  this.options.build.transpile = [
    ...this.options.build.transpile,
    'nuxt-preserve-scroll',
  ];
};

module.exports.meta = require('./package.json');
